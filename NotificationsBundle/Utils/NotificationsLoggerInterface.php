<?php

namespace NotificationsBundle\Utils;

interface NotificationsLoggerInterface

{
    /**
     * @param string $task
     * @param string $username
     * @param string $comment
     */
    public function logAction(string $task, string $username, string $comment): void;

    /**
     * @return array
     */
    public function getLogs(): array;
    
    /**
     * @param string $username;
     * @return array
     */
    public function getLogForUser(string $username): array;

}