<?php

namespace NotificationsBundle\Utils;

final class NotificationsLogger implements NotificationsLoggerInterface
{
    private $fileName;
    
    /**
     * If file declared in constructor doesn't exists - simply create one 
     */
    public function __construct($fileName = "notificationsLogs.json")
    {
        if(!file_exists($fileName)){
            fopen($fileName,"w+");
        }
        $this->fileName = $fileName;
    }

    /**
     * Put Data in .json file (its for Collation use - instead of database)
     * @param string $task
     * @param string $username
     * @param string $userComment
     */
    public function logAction(string $task, string $username, string $userComment): void
    {
        $content = json_decode(file_get_contents($this->fileName),true);
        $newContent = ["task" => $task, "username" => $username, "userComment"=>$userComment];
        array_push($content,$newContent);
        file_put_contents($this->fileName,json_encode($content));
    }
    
    /**
     * Get Data from .json file (its for Collation use - instead of database) 
     * @return array
     */
    public function getLogs(): array
    {
        return json_decode(file_get_contents($this->fileName));
    }

     /**
      * Get Data from .json file for particular user(username)
     * @param string $username;
     * @return array
     */
    public function getLogForUser(string $username): array
    {
        $result = [];
        $input = json_decode(file_get_contents($this->fileName),true);
        foreach($input as $log){
            if($log['username'] === $username){
                array_push($result,$log);
            }
        }
        return $result;
    }
}