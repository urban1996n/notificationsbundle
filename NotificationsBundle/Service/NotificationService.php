<?php

namespace NotificationsBundle\Service;

use NotificationsBundle\Entity\User;
use NotificationsBundle\Entity\Notification;
use NotificationsBundle\Repository\NotificationRepositoryInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use NotificationsBundle\Utils\NotificationsLogger;
    
/**
 * Class NotificationService
 * @package NotificationsBundle\Service
 */
final class NotificationService
{
    /**
     * @var NotificationRepositoryInterface
     */
    private $notificationRepository;

    /**
     * NotificationService constructor.
     * @param NotificationRepositoryInterface $notificationRepository
     */
    public function __construct(NotificationRepositoryInterface $notificationRepository){
        $this->notificationRepository = $notificationRepository;
        $this->refreshNotifications();
    }

    /**
     * @param int $notificationId
     * @return Notification
     * @throws EntityNotFoundException
     */
    public function getNotification(int $notificationId): Notification
    {
        $notification = $this->notificationRepository->findById($notificationId);
        if (!$notification) {
            throw new EntityNotFoundException('Notification has not been found');
        }
        return $notification;
    }

    /**
     * @param User $user
     * @return array|null
     */
    public function getAllNotifications(): ?array
    {
        return $this->notificationRepository->findAll();
    }

    /**
     * @param User $user
     * @return array|null
     */
    public function getNotificationsForUser(User $user): ?array
    {
        return $this->notificationRepository->findByUser($user);
    }

    /**
     * @param User $user
     * @param string $content
     * @param int $priority
     * @param int $repeatability
     * @param \DateTime $startDate
     * @return Notification
     */
    public function addNotification(User $user, string $content, int $priority, int $repeatability, \DateTime $startDate): Notification
    {
        $notification = new Notification();
        $notification->setContent($content);
        $notification->setPriority($priority);
        $notification->setUser($user);
        $notification->setRepeatability($repeatability);
        $notification->setStartDate($startDate);
        if(new \DateTime() >= $startDate){
            $notification->setActive(true);
        }else{
            $notification->setActive(false);
        }

        $this->notificationRepository->save($notification);

        return $notification;
    }

    /**
     * @param int $notificationId
     * @param User $user
     * @param string $content
     * @param int $priority
     * @param int $repeatability
     * @param \DateTime $startDate
     * @return Notification
     * @throws EntityNotFoundException
     */
    public function updateNotification(int $notificationId, User $user, string $content, int $priority, int $repeatability, \DateTime $startDate): Notification
    {
        $notification = $this->notificationRepository->findById($notificationId);
        if (!$notification) {
            throw new EntityNotFoundException('Notification has not been found');            
        }
        $notification->setContent($content);
        $notification->setPriority($priority);
        $notification->setUser($user);
        $notification->setRepeatability($repeatability);
        $notification->setStartDate($startDate);
        $notification->setActive($active);
        $this->notificationRepository->save($notification);

        return $notification;
    }

    /**
     * @param int $notificationId
     * @throws EntityNotFoundException
     */
    public function deleteNotification(int $notificationId): void
    {
        $notification = $this->notificationRepository->findById($notificationId);
        if (!$notification) {
            throw new EntityNotFoundException('Notification has not been found');            
        }

        $this->notificationRepository->delete($notification);
    }
    
    /**
     * Setting notification's Active property to false, so it wont appear in index page
     * 
     * @param int $notificationId
     * @throws EntityNotFoundException
     */
    public function setDone(int $notificationId, string $username, string $comment): void
    {
        $notification = $this->notificationRepository->findById($notificationId);
        if (!$notification) {
            throw new EntityNotFoundException('Notification has not been found');            
        }
        $this->notificationRepository->setDone($notification);
        
        $logger = new NotificationsLogger();
        $logger->logAction($notification->getContent(),$username,$comment);
    }

    /**
     * Activates particular Notifications, basing on Repeatability and StartDate properties, so user can see it as Active in index page, and make it done.
     */
    public function refreshNotifications(): void
    {
        foreach($this->getAllNotifications() as $notification){
            
            if(!$notification->getActive() && $notification->getLastActive() === null && new \DateTime() >= $notification->getStartDate()){
                $this->notificationRepository->Activate($notification);
            }

            if($notification->getRepeatability() > 0 && $notification->getLastActive() !== null && new \DateTime() >= $notification->getLastActive()->modify('+' . $notification->getRepeatability() .' days')){
                $this->notificationRepository->Activate($notification);
            }
        }
    }
}
