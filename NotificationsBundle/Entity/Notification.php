<?php

namespace NotificationsBundle\Entity;

class Notification{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $repeat;

    /**
     * @var \DateTime
     */
    private $start_date;

    /**
     * @var \DateTime
     */
    private $last_active;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $last_done;

    /**
     * @var User
     */
    private $User;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Notification
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Notification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set repeat
     *
     * @param integer $repeat
     *
     * @return Notification
     */
    public function setRepeat($repeat)
    {
        $this->repeat = $repeat;

        return $this;
    }

    /**
     * Get repeat
     *
     * @return integer
     */
    public function getRepeat()
    {
        return $this->repeat;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Notification
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set lastActive
     *
     * @param \DateTime $lastActive
     *
     * @return Notification
     */
    public function setLastActive($lastActive)
    {
        $this->last_active = $lastActive;

        return $this;
    }

    /**
     * Get lastActive
     *
     * @return \DateTime
     */
    public function getLastActive()
    {
        return $this->last_active;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Notification
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set lastDone
     *
     * @param \DateTime $lastDone
     *
     * @return Notification
     */
    public function setLastDone($lastDone)
    {
        $this->last_done = $lastDone;

        return $this;
    }

    /**
     * Get lastDone
     *
     * @return \DateTime
     */
    public function getLastDone()
    {
        return $this->last_done;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Notification
     */
    public function setUser(User $user = null)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->User;
    }
    /**
     * @var integer
     */
    private $priority;


    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return Notification
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }
    /**
     * @var integer
     */
    private $repeatability;


    /**
     * Set repeatability
     *
     * @param integer $repeatability
     *
     * @return Notification
     */
    public function setRepeatability($repeatability)
    {
        $this->repeatability = $repeatability;

        return $this;
    }

    /**
     * Get repeatability
     *
     * @return integer
     */
    public function getRepeatability()
    {
        return $this->repeatability;
    }
}
