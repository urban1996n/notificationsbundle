<?php

namespace NotificationsBundle\Repository;

use NotificationsBundle\Entity\User;
use NotificationsBundle\Entity\Notification;
use NotificationsBundle\Repository\NotificationRepositoryInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ArticleRepository
 * @package App\Infrastructure\Repository
 */
final class NotificationRepository implements NotificationRepositoryInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ObjectRepository
     */
    private $objectRepository;

    /**
     * ArticleRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->objectRepository = $this->entityManager->getRepository(Notification::class);        
    }

    /**
     * @param int $notificationId
     * @return Notification
     */
    public function findById(int $notificationId): ?Notification
    {
        return $this->objectRepository->find($notificationId);
    }

    /**
     * @param User $user
     * @return array | null
     */
    public function findByUser(User $user): ?array
    {
        return $this->objectRepository->findBy(["User" => $user]);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->objectRepository->findAll();
    }

    /**
    * @param Notification $notification
    */
    public function save(Notification $notification): void
    {
        $this->entityManager->persist($notification);
        $this->entityManager->flush();
    }

    /**
    * @param Notification $notification
    */
    public function delete(Notification $notification): void
    {
        $this->entityManager->remove($notification);
        $this->entityManager->flush();
    }

    /**
     * @param Notification $notification 
     */
    public function setDone(Notification $notification): void
    {
        $notification->setActive(false);
        $notification->setLastDone(new \DateTime());
        $this->save($notification);
    }
    
    /**
     * @param Notification $notification
     */
    public function Activate(Notification $notification): void
    {
        $notification->setActive(true);
        $notification->setLastActive(new \DateTime());
        $this->save($notification);
    }
}
