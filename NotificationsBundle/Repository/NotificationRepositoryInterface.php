<?php

namespace NotificationsBundle\Repository;

use NotificationsBundle\Entity\Notification;
use NotificationsBundle\Entity\User;

Interface NotificationRepositoryInterface
{
    /**
    * @param int $notificationId
    * @return Notification | null
    */
    public function findById(int $notificationId) : ?Notification;

    /**
     * @param User $user
     * @return array | null
     */
    public function findByUser(User $user): ?array;

    /**
    * @param int $notificationId
    * @return array | null
    */
    public function findAll() : ?array;

    /**
    * @param Notification $notification
    */
    public function save(Notification $notification): void;

    /**
    * @param Notification $notification
    */
    public function delete(Notification $notification): void;
    
    /**
    * @param Notification $notification
    */
    public function setDone(Notification $notification): void;
    
    /**
    * @param Notification $notification
    */
    public function Activate(Notification $notification): void;



}