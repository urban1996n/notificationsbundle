<?php

namespace NotificationsBundle\Controller;

use NotificationsBundle\Utils\NotificationsLogger;
use NotificationsBundle\Service\NotificationService;
use NotificationsBundle\Entity\Test;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Twig\Environment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NotificationsController extends Controller
{
  /**
   * @var NotificationService
   */
  private $notificationService;
  /**
   * @var Environment
   */
  private $twig;
  /**
   * @var NotificationsLogger
   */
  private $logger;
  
  /**
   * @param NotificationService $notificationService
   */
  public function __construct(NotificationService $notificationService, Environment $twig, NotificationsLogger $logger)
  {
    $this->notificationService = $notificationService;
    $this->twig = $twig;
    $this->logger = $logger;
  }

  /**
    * @Route("/notifications", name="notifications_index")
  */
  public function index()
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
    {
      $twigFile = "admin.index.html.twig";
      $notifications = $this->notificationService->getAllNotifications();   
    }else{
      $priorities = [];
      $notifications = $this->notificationService->getNotificationsForUser($this->getUser());
      
      foreach($notifications as $notification){
        array_push($priorities,$notification->getPriority());
      }

      if(in_array(1,$priorities)){
        $notificationsType = "warning";
      }elseif(in_array(2,$priorities)){
        $notificationsType = "alert";
      }else{
        $notificationsType = "info";
      }

      $twigFile = "index.html.twig";
    }
    
    $content = $this->twig->render(
      "notification/{$twigFile}",
      ["notifications" => $notifications, "notificationsType" => $notificationsType]
    );

    return new Response($content);
  }

  /**
    * @Security("has_role('ROLE_ADMIN')")
    * @Route("/notifications/collation",  name="notifications_collation")
  */
  public function allNotificationsCollation(){
    $collation = $this->logger->getLogs();
    
    $content = $this->twig->render(
      "notification/collation.html.twig",
      ["collation" => $collation]
    );

    return new Response($content);
  }
  
  /**
    * @Security("has_role('ROLE_ADMIN')")
    * @Route("/notifications/userCollation/{username}",  name="notifications_user_collation")
  */
  public function userNotificationsCollation($username){
    $collation = $this->logger->getLogForUser($username);
    
    $content = $this->twig->render(
      "notification/collation.html.twig",
      ["collation" => $collation]
    );
    
    return new Response($content);
  }

  /**
    * @Security("has_role('ROLE_ADMIN')")
    * @Route("/notifications/add", methods={"POST"}, name="notifications_add")
  */
  public function addNotification(Request $request)
  {
    $user = $this->getDoctrine()->getManager()->getRepository('NotificationsBundle\Entity\User')->find($request->get('userId'));
    $nots = $this->notificationService->addNotification($user, $request->get('content'), $request->get('priority'), $request->get('repeatability'), $request->get('startDate'));
    return new Response(Response::HTTP_CREATED);
  }

  /**
    * @Security("has_role('ROLE_ADMIN')")
    * @Route("/notifications/{notificationId}", methods={"PUT"}, name="notifications_edit")
  */
  public function editNotification(Request $request, int $notificationId)
  {
    $nots = $this->notificationService->updateNotification($notificationId, $user, $request->get('content'), $request->get('priority'), $request->get('repeatability'), $request->get('startDate'));    
    return new Response(Response::HTTP_OK);
  }

  /**
    * @Security("has_role('ROLE_ADMIN')")
    * @Route("/notifications/{notificationId}", methods={"DELETE"}, name="notifications_delete")
  */
  public function deleteNotification(Request $request, int $notificationId)
  {
    $nots = $this->notificationService->deleteNotification($notificationId);
    return new Response(Response::HTTP_NO_CONTENT);
  }
  
  /**
    * @Route("/notifications/complete/{notificationId}", methods={"PUT"},  name="notifications_complete")
  */
  public function setDone(Request $request, int $notificationId)
  {
    $this->notificationService->setDone($notificationId, $this->getUser()->getUserName(), $request->get('comment'));
    return new Response(Response::HTTP_OK);
  }
}