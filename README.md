# NotificationsBundle




Symfony 3.4 Semi-bundle:
You need to provide Users Entity, such as Twig files and enable this budle as service by adding theese lines to "services.yml":
 
    _defaults:
        autowire: true
        autoconfigure: true
        public: false

    NotificationsBundle\:
        resource: '../../src/Bundle/NotificationsBundle/*'
        exclude: '../../src/Bundle/NotificationsBundle/{Entity}'

    NotificationsBundle\Controller\:
        resource: '../../src/Bundle/NotificationsBundle/Controller'
        public: true
        tags: ['controller.service_arguments']
        
Used by third-party application to give users notifications, and to manage them from users with "ROLE_ADMIN" granted.

STILL IN DEVELOPMENT.